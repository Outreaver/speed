﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpeedcubingTimer.Constants;

namespace SpeedcubingTimer.Utils
{
    public static class ScrambleUtils
    {
        public static string GenerateScramble()
        {
            var scramble = string.Empty;
            var randomAddition = string.Empty;
            var allMoves = Enum.GetValues(typeof(CubeMoves)).Cast<CubeMoves>().ToArray();
            CubeMoves[] availableMoves;
            CubeMoves lastMove, randomMove = GetRandomItem(allMoves);

            for (int i = 0; i < 25; i++)
            {
                lastMove = randomMove;
                availableMoves = allMoves.Where(x => x != lastMove).ToArray();
                randomMove = GetRandomItem(availableMoves);
                randomAddition = GetRandomItem(Addition.AllAdditions);
                scramble += randomMove + randomAddition + " ";
            }
            return scramble.Trim();
        }

        private static T GetRandomItem<T>(T[] array)
        {
            var random = new Random();
            int itemIndex = random.Next(array.Length);
            return array[itemIndex];
        }
    }
}
