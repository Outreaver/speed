﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeedcubingTimer.Utils
{
    public static class TimerUtils
    {
        public static string GetTimeInSeconds(TimeSpan timeSpan)
        {
            if (timeSpan.Minutes > 0)
                return String.Format("{0:00}:{1:00}.{2:00}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds / 10);
            else
                return String.Format("{1:00}.{2:00}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds / 10);
        }
    }
}
