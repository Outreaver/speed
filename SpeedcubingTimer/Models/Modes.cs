﻿using System;
using System.Collections.Generic;

namespace SpeedcubingTimer.Models
{
    public partial class Modes
    {
        public Modes()
        {
            Times = new HashSet<Times>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Times> Times { get; set; }
    }
}
