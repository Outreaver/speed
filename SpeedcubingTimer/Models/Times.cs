﻿using SpeedcubingTimer.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpeedcubingTimer.Models
{
    public partial class Times
    {
        public int Id { get; set; }
        public TimeSpan Time { get; set; }
        public DateTime Date { get; set; }
        public int Cube { get; set; }
        public int Mode { get; set; }
        public string Scramble { get; set; }
        [Column("+2")]
        public bool? Plus2 { get; set; }
        public bool? Dnf { get; set; }
        public TimeSpan? Ao5 { get; set; }
        public TimeSpan? Ao12 { get; set; }
        public TimeSpan? Ao100 { get; set; }

        public virtual Cubes CubeNavigation { get; set; }
        public virtual Modes ModeNavigation { get; set; }

        public string TimeFormatted
        {
            get
            {
                if ((bool)Plus2)
                    return TimerUtils.GetTimeInSeconds(Time) + " (+2)";
                else if ((bool)Dnf)
                    return TimerUtils.GetTimeInSeconds(Time) + " (DNF)";
                return TimerUtils.GetTimeInSeconds(Time);
            }
        }

        public string DateFormatted
        {
            get => string.Format("{0:dd/MM/yy H:mm:ss}", Date);
        }

        public string Ao5Formatted
        {
            get
            {
                if (Ao5 != null)
                    return TimerUtils.GetTimeInSeconds((TimeSpan)Ao5);
                return string.Empty;
            }
        }

        public string Ao12Formatted
        {
            get
            {
                if (Ao5 != null)
                    return TimerUtils.GetTimeInSeconds((TimeSpan)Ao5);
                return string.Empty;
            }
        }

        public string Ao100Formatted
        {
            get
            {
                if (Ao5 != null)
                    return TimerUtils.GetTimeInSeconds((TimeSpan)Ao5);
                return string.Empty;
            }
        }
    }
}
