﻿using System;
using System.Collections.Generic;

namespace SpeedcubingTimer.Models
{
    public partial class Cubes
    {
        public Cubes()
        {
            Times = new HashSet<Times>();
        }

        public int Id { get; set; }
        public string Manufacturer { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Times> Times { get; set; }
    }
}
