﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SpeedcubingTimer.Models
{
    public partial class SpeedcubingTimerContext : DbContext
    {
        public SpeedcubingTimerContext()
        {
        }

        public SpeedcubingTimerContext(DbContextOptions<SpeedcubingTimerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cubes> Cubes { get; set; }
        public virtual DbSet<Modes> Modes { get; set; }
        public virtual DbSet<Times> Times { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-5PK042G\\SQLSERVER;Database=SpeedcubingTimer;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cubes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Manufacturer)
                    .IsRequired()
                    .HasColumnName("manufacturer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Modes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Times>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ao100).HasColumnName("ao100");

                entity.Property(e => e.Ao12).HasColumnName("ao12");

                entity.Property(e => e.Ao5).HasColumnName("ao5");

                entity.Property(e => e.Cube).HasColumnName("cube");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dnf).HasColumnName("dnf");

                entity.Property(e => e.Mode).HasColumnName("mode");

                entity.Property(e => e.Scramble)
                    .HasColumnName("scramble")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Plus2).HasColumnName("+2");

                entity.HasOne(d => d.CubeNavigation)
                    .WithMany(p => p.Times)
                    .HasForeignKey(d => d.Cube)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Times__cube__778AC167");

                entity.HasOne(d => d.ModeNavigation)
                    .WithMany(p => p.Times)
                    .HasForeignKey(d => d.Mode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Times__mode__787EE5A0");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
