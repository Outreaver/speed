﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Controls;
using System.Windows.Threading;
using SpeedcubingTimer.Utils;

namespace SpeedcubingTimer
{
    public class Timer
    {
        private DispatcherTimer DispatcherTimer { get; set; }
        private Stopwatch Stopwatch { get; set; }
        private TextBlock TextBlock { get; set; }

        public Timer(TextBlock textBlock)
        {
            DispatcherTimer = new DispatcherTimer();
            DispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            Stopwatch = new Stopwatch();
            DispatcherTimer.Tick += Ticker;
            TextBlock = textBlock;
        }

        public void Start()
        {
            Stopwatch.Start();
            DispatcherTimer.Start();
        }

        public void Stop()
        {
            Stopwatch.Stop();
            DispatcherTimer.Stop();
            TextBlock.Text = TimerUtils.GetTimeInSeconds(Stopwatch.Elapsed);
        }

        public string StopAndReset()
        {
            Stopwatch.Stop();
            var result = TimerUtils.GetTimeInSeconds(Stopwatch.Elapsed);
            Stopwatch.Reset();
            DispatcherTimer.Stop();
            return result;
        }

        public TimeSpan GetTime()
        {
            return Stopwatch.Elapsed;
        }

        public string GetTimeString()
        {
            return TimerUtils.GetTimeInSeconds(Stopwatch.Elapsed);
        }

        private void Ticker(object sender, EventArgs e)
        {
            var timeSpan = Stopwatch.Elapsed;
            TextBlock.Text = TimerUtils.GetTimeInSeconds(timeSpan);
        }
    }
}
