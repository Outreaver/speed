﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SpeedcubingTimer.Models;
using SpeedcubingTimer.Utils;

namespace SpeedcubingTimer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Timer _timer { get; set; }
        private SpeedcubingTimerContext _speedcubingTimerContext { get; set; }
        public MainWindow()
        {
            _speedcubingTimerContext = new SpeedcubingTimerContext();
            InitializeComponent();
            LoadComboBoxes();
            LoadBestTimes();
            LoadDataGridTimes();
            _timer = new Timer(TextBlockMainTime);
            _timer.Start();
            TextBlockStandardDeviation.Text = StandardDeviation();
        }

        private async void KeyDownMainWindow(object sender, KeyEventArgs e)
        {
            _timer.Stop();
            //var result = new Times(_timer.GetTime(), 1, 1);
            //_speedcubingTimerContext.Times.Add(result);
            //DataGridTimesResult.Items.Insert(0, new TimeResult(_timer.GetTimeString()));
            DataGridTimesResult.Items.RemoveAt(DataGridTimesResult.Items.Count-1);
            TextBlockScramble.Text = ScrambleUtils.GenerateScramble();
        }

        private void LoadComboBoxes()
        {
            ComboBoxMode.ItemsSource = _speedcubingTimerContext.Modes.Select(x => x.Name).ToList();
            ComboBoxCube.ItemsSource = _speedcubingTimerContext.Cubes.Select(x => $"{x.Manufacturer}, {x.Name}").ToList();
        }

        private void LoadDataGridTimes()
        {
            var times = _speedcubingTimerContext.Times.OrderByDescending(x => x.Id).Take(30).ToArray();
            foreach (var time in times)
                DataGridTimesResult.Items.Add(time);
        }

        private void LoadBestTimes()
        {
            var ao5 = _speedcubingTimerContext.Times.Select(x => x.Ao5).Min();
            var ao12 = _speedcubingTimerContext.Times.Select(x => x.Ao12).Min();
            var ao100 = _speedcubingTimerContext.Times.Select(x => x.Ao100).Min();
            TextBlockAo5Best.Text = ao5 != null ? TimerUtils.GetTimeInSeconds((TimeSpan)ao5) : string.Empty;
            TextBlockAo12Best.Text = ao12 != null ? TimerUtils.GetTimeInSeconds((TimeSpan)ao12) : string.Empty;
            TextBlockAo100Best.Text = ao100 != null ? TimerUtils.GetTimeInSeconds((TimeSpan)ao100) : string.Empty;
        }

        private string StandardDeviation()
        {
            var times = _speedcubingTimerContext.Times.Select(x => x.Time).ToArray();
            var avg = times.Average(x => x.Ticks);
            var squaresSum = times.Sum(x => (x.Ticks - avg) * (x.Ticks - avg));
            var standardDeviation = Math.Sqrt(squaresSum / times.Length);
            return TimerUtils.GetTimeInSeconds(new TimeSpan(Convert.ToInt64(standardDeviation)));
        }
    }
}
