﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeedcubingTimer.Constants
{
    public enum CubeMoves
    {
        R,
        L,
        U,
        D,
        F,
        B
    }

    public static class Addition
    {
        public const string Empty = "";
        public const string Prim = "\'";
        public const string Double = "2";

        public static readonly string[] AllAdditions = {Empty, Prim, Double };
    }
}
